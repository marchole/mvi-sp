# mvi-sp
## Image search and understanding
- The goal of the work is to learn neural network to predict user interactions embeddings for e-shop using convolutional neural net. The task is to try different pretrained CNN models to know which one is the best for given problem. 
- Complete data cannot be provided due to NDA, still demonstration examples are provided in notebook and report
- run file as usual jupyter notebook